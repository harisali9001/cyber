
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('Sent Emails'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('photo'); ?></th>
                      <th><?php echo get_phrase('name'); ?></th>
                      <th><?php echo get_phrase('email'); ?></th>
                      <th><?php echo get_phrase('enrolled_trainings'); ?></th>
                      <th><?php echo get_phrase('status'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $key = 0; //print_r($users);
                       foreach ($emails as  $user): $key++; ?>
                          <tr>
                              <td><?php echo $key; ?></td>
                              <td>
                                  <img src="<?php echo $this->user_model->get_user_image_url($user->id);?>" alt="" height="50" width="50" class="img-fluid rounded-circle img-thumbnail">
                              </td>
                              <td><?php echo $user->first_name.' '.$user->last_name; ?>
                                
                              </td>
                              <td><?php echo $user->email; ?></td>
                              <td>
                                 <?php echo ucwords($user->course_name); ?>
                              </td>

                              <td>
                                
                                        <span class="badge badge-success-lighten">email sent</span>

                                  
                              </td>  

                             
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
