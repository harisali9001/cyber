<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                <a href = "<?php echo site_url('admin/user_form/add_instructor_form/'.$org_id); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_Instructor'); ?></a>
                &nbsp;
                <a href = "<?php echo site_url('admin/organization'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-previous"></i><?php echo get_phrase('Back'); ?></a>
            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('instructors'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('photo'); ?></th>
                      <th><?php echo get_phrase('name'); ?></th>
                      <th><?php echo get_phrase('email'); ?></th>
                      <th><?php echo get_phrase('phone'); ?></th>
                      <th><?php echo get_phrase('courses_assign'); ?></th>
                      <th><?php echo get_phrase('organization'); ?></th>
                      <th><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($instructors as $key => $user): ?>
                        <tr>
                            <td><?php echo $key+1; ?></td>
                            <td>
                                <img src="<?php echo $this->user_model->get_user_image_url($user->id);?>" alt="" height="50" width="50" class="img-fluid rounded-circle img-thumbnail">
                            </td>
                            <td><?php echo ucwords($user->first_name.' '.$user->last_name); ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td><?php echo $user->phone; ?></td>
                            <td><ul><?php 
                                  $filter ="user_id = ".$user->id;
                                  $courses = $this->crud_model->get_courses_all($filter);

                                  foreach($courses as $c):

                                    echo "<li>".ucwords($c->title)."</li>";
                                  endforeach;  

                             ?></ul>

 <!-- <a class="btn btn-outline-primary btn-rounded alignToTitle" href="#" onclick="assign_course_modal('asfasdf');"><i class="mdi mdi-plus"></i><?php echo get_phrase('courses'); ?></a> -->
                           </td>
                          
                            <td><span class="badge badge-success-lighten"><b><?php echo ucwords($user->org_name); ?></b></span></td>
                            <td>
                                <div class="dropright dropright">
                                  <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="mdi mdi-dots-vertical"></i>
                                  </button>
                                  <ul class="dropdown-menu">
                                      
                                      <li><a class="dropdown-item" href="<?php echo site_url('admin/instructor_form/edit_instructor_form/'.$user->id.'/'.$org_id) ?>"><?php echo get_phrase('edit'); ?></a></li>
                                      <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/instructors/delete/'.$user->id.'?og='.$org_id); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                  </ul>
                              </div>
                            </td>
                        </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
