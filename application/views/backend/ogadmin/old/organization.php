<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                <a href = "<?php echo site_url('admin/add_organization/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_organization'); ?></a>
                


            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('organizations'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('organization_name'); ?></th>
                      <th><?php echo get_phrase('domain_name'); ?></th>
                      <th><?php echo get_phrase('created_on'); ?></th>
                      <th><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($organizations as $key => $org): ?>
                        <tr>
                            <td><?php echo $key+1; ?></td>
                            <td><?php echo ucwords($org->name); ?></td>
                            <td><?php echo ucwords($org->domain); ?></td>
                            <td><?php echo date('Y-m-d', $org->created_on); ?></td>
                            <td>
                                <div class="dropright dropright">
 
                              <a href="<?php echo site_url('admin/ogadmins?og='.$org->org_id); ?>">
                                  <button type="button" class="btn btn-warning">Admin</button>
                              </a>

                              <a href="<?php echo site_url('admin/instructors?og='.$org->org_id); ?>">
                                    <button type="button" class="btn btn-success">Trainers</button>
                              </a>

                              <a href="<?php echo site_url('admin/candidates?og='.$org->org_id); ?>">
                                  <button type="button" class="btn btn-primary">Candidates</button>
                              </a> 
                                  &nbsp;
                                  <button type="button" class="btn btn-sm btn-outline-danger btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="mdi mdi-dots-vertical"></i>
                                  </button>
 
                                  <ul class="dropdown-menu">
                                      
                                      <li><a class="dropdown-item" href="<?php echo site_url('admin/edit_organization/'.$org->org_id) ?>"><?php echo get_phrase('edit'); ?></a></li>
                                      <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/delete_organization/'.$org->org_id); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                  </ul>
                            </div>
                            </td>
                        </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
