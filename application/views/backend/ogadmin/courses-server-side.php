<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
               
               
            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('courses'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="course-datatable" class="table table-striped table-centered mb-0">
                         <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('title'); ?></th>
                                <th><?php echo get_phrase('category'); ?></th>
                                <th><?php echo get_phrase('lesson_and_section'); ?></th>
                                <th><?php echo get_phrase('assign_instructor'); ?></th>
                                <th><?php echo get_phrase('status'); ?></th>
                                <th><?php echo get_phrase('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                               <?php 
                               $cnt =0 ;
                               foreach($courses as $course): $cnt++; 
                $category_details = $this->crud_model->get_category_details_by_id($course->sub_category_id)->row_array();
                $instructor_details = $this->user_model->get_all_user($course->user_id)->row_array();


                $sections = $this->crud_model->get_section_lesson_list('section', $course->id);

                $lesson = $this->crud_model->get_section_lesson_list('lesson', $course->id);

                                ?> 
                            <tr>
                                <td><?php echo $cnt; ?></td>
                                <td><b><?php echo ucwords($course->title); ?></b></td>
                                <td><b><?php echo ucwords($category_details['name']); ?></b></td>
                                <td><?php  $sections_list = '
            <small class="text-muted"><b>'.get_phrase('total_section').'</b>: '.$sections->total_section.'</small><br>
            <small class="text-muted"><b>'.get_phrase('total_lesson').'</b>: '.$lesson->total_lesson.'</small><br>';  echo $sections_list;?></td>
                                <td><?php $instructor_ = '<span class="badge-success-lighten">'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</span>'; 
                                    echo $instructor_
                                ?></td>
                                <td><?php 

                                    if($course->status == "active"){

                                            echo '<span class="badge-success-lighten">active</span>';     
                                    }else if($course->status == "reject"){

                                            echo '<span class="badge-danger-lighten">reject</span>';
                                            echo '<p style="color:brown;">Remarks: '.ucfirst($course->remarks).'</p>';     
                                    }else{

                                            echo '<span class="badge-danger">in-active</span>';     

                                    }

                                 ?></td>
                                <td><a class="btn btn-warning"href="<?php echo site_url('OgAdmin/course_form/course_edit/'.$course->id); ?>">edit</a></td>
                            </tr>    

                               <?php endforeach; ?> 

                        </tbody>   
                    </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
