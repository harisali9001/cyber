<?php
	$status_wise_courses = $this->crud_model->get_status_wise_courses();
 ?>
<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu left-side-menu-detached">
	<div class="leftbar-user">
		<a href="javascript: void(0);">
			<img src="<?php echo $this->user_model->get_user_image_url($this->session->userdata('user_id')); ?>" alt="user-image" height="42" class="rounded-circle shadow-sm">
			<?php
			$admin_details = $this->user_model->get_all_user($this->session->userdata('user_id'))->row_array();
			?>
			<span class="leftbar-user-name"><?php echo $admin_details['first_name'].' '.$admin_details['last_name']; ?></span>
		</a>
	</div>

	<!--- Sidemenu -->
		<ul class="metismenu side-nav side-nav-light">

			<li class="side-nav-title side-nav-item"><?php echo get_phrase('navigation'); ?></li>

			<li class="side-nav-item <?php if ($page_name == 'dashboard')echo 'active';?>">
				<a href="<?php echo site_url('OgAdmin/dashboard'); ?>" class="side-nav-link">
					<i class="dripicons-view-apps"></i>
					<span><?php echo get_phrase('events-calendar'); ?></span>
				</a>
			</li>

			

			<li class="side-nav-item">
				<a href="<?php echo site_url('OgAdmin/candidate'); ?>" class="side-nav-link <?php if ($page_name == 'manage_candidate')echo 'active';?>">
					<i class="dripicons-archive"></i>
					<span><?php echo get_phrase('manage_candidate'); ?></span>
				</a>
			</li>

			<li class="side-nav-item">
				<a href="<?php echo site_url('OgAdmin/courses'); ?>" class="side-nav-link <?php if ($page_name == 'courses')echo 'active';?>">
					<i class="dripicons-archive"></i>
					<span><?php echo get_phrase('courses'); ?></span>
				</a>
			</li>

			<!-- <li class="side-nav-item">
				<a href="<?php echo site_url('OgAdmin/reports'); ?>" class="side-nav-link <?php if ($page_name == 'reports')echo 'active';?>">
					<i class="mdi mdi-incognito"></i>
					<span><?php echo get_phrase('reports'); ?></span>
				</a>
			</li>
			 -->
			<li class="side-nav-item">
				<a href="<?php echo site_url('OgAdmin/manage_profile'); ?>" class="side-nav-link <?php if ($page_name == 'profile')echo 'active';?>">
					<i class="mdi mdi-incognito"></i>
					<span><?php echo get_phrase('my_profile'); ?></span>
				</a>
			</li>
			

	    </ul>
</div>
