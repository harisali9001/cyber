<link href="<?php echo base_url('assets/events/core/main.css'); ?>" rel='stylesheet' />
<link href="<?php echo base_url('assets/events/daygrid/main.css'); ?>" rel='stylesheet' />
<script src="<?php echo base_url('assets/events/core/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/interaction/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/daygrid/main.js'); ?>"></script>
<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      header: {
        left: 'prevYear,prev,next,nextYear today',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      },
      defaultDate: '<?php echo date("Y-m-d"); ?>',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
         <?php foreach($tests  as $t){  $exp = explode(" ",$t->training_date); ?>    

        {
          title: '<?php echo ucwords($t->title);?>',
          url: '#',
          start: '<?php echo $exp[0]; ?>'
        },
        
        <?php } ?>  
  
      ]
    });

    calendar.render();
  });

</script>
<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>


<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('Training-Event-Calendar'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<?php
    
    $filter = "org_id = ".$this->session->userdata('org');
    $number_of_instructors = $this->user_model->get_totl_organization_instructor($filter);
    

    $std_filter = "role_id =3 and org_id = ".$this->session->userdata('org');
    $number_of_students = $this->user_model->get_totl_users($std_filter);


    $course_filter = "co.org_id = ".$this->session->userdata('org');
    $number_of_courses = $this->crud_model->get_totl_number_of_courses($course_filter);

    //print_r($number_of_instructors);
?>
<div class="row">
    <div class="col-12">
        <div class="card widget-inline">
            <div class="card-body p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-xl-3">
                        <a href="" class="text-secondary">
                            <div class="card shadow-none m-0">
                                <div class="card-body text-center">
                                    <i class="dripicons-archive text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_courses->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_courses'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-3">
                        <a href="" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_instructors->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_trainers'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                   

                    <div class="col-sm-6 col-xl-3">
                        <a href="" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_students->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_student'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                </div> <!-- end row -->
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end col-->
</div>
<div id='calendar'></div>

