<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('Manage Tests').': '.$course_details['title']; ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('Test Manager'); ?>
                    <a href="<?php echo site_url('user/all_tests/'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm ml-1" ><?php echo get_phrase('View All Tests'); ?> <i class="mdi mdi-arrow-right"></i> </a>
                    <a href="<?php echo site_url('user/tests'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"><i class=" mdi mdi-keyboard-backspace"></i> <?php echo get_phrase('create test'); ?></a>
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                        <form class="required-form" action="<?php echo site_url('user/save_test/save/'.$course_id); ?>" method="post" enctype="multipart/form-data">
 
                            <input type="hidden" name="total" id="total">    
                            <input type="hidden" name="identity" id="identity" value="<?php echo $test_details->identity; ?>">    
                            <div id="basicwizard">
                                

                                <div class="tab-content b-0 mb-0">
                                    <div class="tab-pane" id="curriculum">
                                        <?php include 'curriculum.php'; ?>
                                    </div>

                                <div>
                                    <div class="row justify-content-center">
                                        <div class="col-xl-8">

                                            <div class="form-group row mb-3">
                                                <label class="col-md-2 col-form-label" for="sub_category_id"><?php echo get_phrase('Select Course'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id" required>
                                                        <option value=""><?php echo get_phrase('select_option'); ?></option>
                                                        <?php foreach($courses as $course): ?>
                                                            <optgroup label="">
                                                                <option value="<?php echo $course['id']; ?>" <?php if($test_details->course_id == $course['id']){ echo "selected";}?>><?php echo $course['title']; ?></option>
                                                            </optgroup>
                                                    <?php endforeach; ?>
                                                </select>
                                              </div>
                                            </div>
    

                                            <div class="form-group row mb-3">
                                                <label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Title'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" id="title" name = "title" placeholder="<?php echo get_phrase('enter_title'); ?>" value="<?php echo $test_details->title?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Question'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" id="question" name = "question" placeholder="<?php echo get_phrase('enter_question'); ?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-2 col-form-label" for="sub_category_id"><?php echo get_phrase('Answer Options'); ?><span class="required">*</span></label>
                                                <div class="col-md-10">
                                                    <select class="form-control select2" data-toggle="select2" name="ans_options" id="ans_options" required onchange="GetOptions(this.value)">
                                                        <option value=""><?php echo get_phrase('select_option'); ?></option>
                                                        <?php for($i=1; $i<=5; $i++): ?>
                                                            <optgroup label="">
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            </optgroup>
                                                    <?php endfor; ?>
                                                </select>
                                              </div>
                                            </div>

                                            <div class="form-group row mb-3" id="replace_me">
                                                
                                              
                                                        <label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer'); ?>1<span class="required">*</span></label>
                                                        
                                                        <div class="col-md-7">

                                                            <input type="text" class="form-control" id="ans1" name = "ans1" placeholder="<?php echo get_phrase('enter_answer'); ?>" required>

                                                        </div>

                                                        <div class="col-md-3">

                                                            <input type="checkbox" class="">

                                                        </div>
                                                

                                            </div>



                                        <div class="mb-3 mt-3">
                                            <button type="button" class="btn btn-primary text-center" onclick="checkRequiredFields()"><?php echo get_phrase('submit'); ?></button>
                                        </div>


                                            
                                            
                                            
                                       
                                            
                                            
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div> <!-- end tab pane -->


<div class="table-responsive-sm mt-4">
                <?php if (count($test_list) > 0): ?>
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('Course'); ?></th>
                                <th><?php echo get_phrase('title'); ?></th>
                                <th><?php echo get_phrase('Question'); ?></th>
                                <th><?php echo get_phrase('Total Options'); ?></th>
                                <th><?php echo get_phrase('Options'); ?></th>
                                <th><?php echo get_phrase('Correct options'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $key=0;

                             foreach ($test_list as  $T):
                                $options = $this->crud_model->get_all_options($T->id);
                                $correct_options = $this->crud_model->get_all_options($T->id,1);

                                
                            ?>
                                <tr>
                                    <td><?php echo ++$key; ?></td>
                                    <td>
                                        <strong><a href="#"><?php echo ellipsis($T->c_name); ?></a></strong>                                       
                                    </td>
                                    <td>
                                        <strong><?php echo $T->title; ?></strong>
                                    </td>
                                     <td>
                                        <strong class="badge badge-dark-lighten"><?php echo $T->question; ?></strong>
                                    </td>
                                    <td>
                                        <strong><a href="#"><?php echo ellipsis($T->number_of_options); ?></a></strong>
                                    </td>
                                    <td>
                                        
                                        <?php
                                            foreach($options as $op){

                                                echo "+<span class='badge badge-dark-lighten'>".$op->options."</span><br/>";
                                            }
                                        ?>
                                        
                                    </td>

                                    <td> 

                                        <?php
                                            foreach($correct_options as $op){

                                                echo "+<span class='badge badge-pink'>".$op->options."</span><br/>";
                                            }
                                        ?>
                                            
                                        </td>
                                   
                                    <td>
                                         <a class="btn btn-danger" href="#" onclick="confirm_modal('<?php echo site_url('user/delete_test/'.$T->id.'/'.$test_details->identity); ?>');"><?php echo get_phrase('delete'); ?></a>
                                          
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php if (count($courses) == 0): ?>
                    <div class="img-fluid w-100 text-center">
                      <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                      <?php echo get_phrase('no_data_found'); ?>
                    </div>
                <?php endif; ?>
            </div>
        

                       
                    </div> <!-- tab-content -->
                </div> <!-- end #progressbarwizard-->
            </form>
        </div>
    </div><!-- end row-->
</div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('user/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function GetOptions(val){

    var data ="";
    for(a=1; a<=val; a++){

        data+='<label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer');?>'+a+'<span class="required">*</span></label><div class="col-md-7"><input type="text" class="form-control" id="ans'+a+'" name = "ans'+a+'" placeholder="<?php echo get_phrase('enter_anwer'); ?>" required></div><div class="col-md-3"><input type="checkbox" name="opt'+a+'" class=""><div class="row">&nbsp;</div></div>';
    }

    document.getElementById("replace_me").innerHTML=data;
    document.getElementById("total").value=a-1;

}


function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
