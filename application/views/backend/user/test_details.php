<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>


<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('Result Test'); ?>
                
                    <a href="<?php echo site_url('user/get_result/'.$identity.'/'.$student_id); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm ml-1" ><?php echo get_phrase('Summary Result'); ?>  </a>&nbsp;
                    <a href="<?php echo site_url('user/test_details/'.$identity.'/'.$student_id); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"> <?php echo get_phrase('Test Details'); ?> </a>&nbsp;
                   
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                    
        
        
              <?php
            $q=0;
            foreach($questions as $QS){ $q++;
        ?>
            <input type="hidden" name="question[]" value="<?php echo $QS->id; ?>">
            <input type="hidden" name="identity" value="<?php echo $QS->identity; ?>">
        <div class="table-responsive-sm mt-4">
              
                <?php //if (count($courses) > 0): ?>
                    <table  class="table table-striped dt-responsive nowrap" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th style="color:#727cf5;"width="75%">Q.<?php echo $q; ?>:   <b><?php echo ucfirst($QS->question);?></b></th>
                               <?php if($q == 1){ ?> 
                                <th style="background-color:#c0eaa5">Right Options</th>
                               <?php }else{ ?>
                                    <th>&nbsp;</th>

                               <?php } ?> 
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                 <td>
                            

                                     <table>
                                         <tr>
                                          <?php $options = $this->crud_model->get_question_options($QS->id); ?>
                                          <?php 
                                            #get my selected options
                                           
                                            $my_option = $this->crud_model->get_my_answer($student_id,$QS->id); 

                                            $my_array = array();
                                            foreach($my_option as $my){

                                                $my_array[] = $my->op_id;
                                            }    

                                          ?>
        
                                    <?php $cnt=0 ;
                                            foreach($options as $OP){  $cnt++;

                                                    $bgcolor = $cnt%2==0?"#f7f8fa":"#dedfe1";  ?>       
                                                              
                                                        <td style="background-color:<?php echo $bgcolor;?>">
                                                            <input type="checkbox" name="opt[<?php echo $QS->id; ?>][<?php echo $cnt;?>]" value="<?php echo $OP->op_id;?>" <?php if(in_array($OP->op_id,$my_array)){ ?> checked <?php } ?> disabled="disabled" > <b><?php echo ucwords($OP->options);?></b></td>
                                                        
                                                   

                                           <?php }//options foreach ?>  
                                            </tr>    
                                             
                                     </table>         
                                </td>

                                <td>

                                      <table>
                                         <tr>
                                          <?php $correct_options = $this->crud_model->get_question_options($QS->id,"correct"); ?>
                                         
        
                                    <?php $cnt=0 ;
                                            foreach($correct_options as $OP){  $cnt++;

                                                    $bgcolor = $cnt%2==0?"#f7f8fa":"#ecf1a7";  ?>       
                                                              
                                                        <td style="background-color:<?php echo $bgcolor;?>">
                                                            <input type="checkbox" checked="true" disabled="disabled" > <b><?php echo ucwords($OP->options);?></b></td>
                                                        
                                                   

                                           <?php }//options foreach ?>  
                                            </tr>    
                                             
                                     </table>  





                                </td>   
                            </tr>    

                            

                            
                        </tbody>
                    </table>


        <?php
            }//foreach questions
        ?>
                
                
            </div>



        
                    </div>
                </div><!-- end row-->
</div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('user/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function GetOptions(val){

    var data ="";
    for(a=1; a<=val; a++){

        data+='<label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer');?>'+a+'<span class="required">*</span></label><div class="col-md-7"><input type="text" class="form-control" id="ans'+a+'" name = "ans'+a+'" placeholder="<?php echo get_phrase('enter_anwer'); ?>" required></div><div class="col-md-3"><input type="checkbox" name="opt'+a+'" class=""><div class="row">&nbsp;</div></div>';
    }

    document.getElementById("replace_me").innerHTML=data;
    document.getElementById("total").value=a-1;

}


function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
