<link href="<?php echo base_url('assets/events/core/main.css'); ?>" rel='stylesheet' />
<link href="<?php echo base_url('assets/events/daygrid/main.css'); ?>" rel='stylesheet' />
<script src="<?php echo base_url('assets/events/core/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/interaction/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/daygrid/main.js'); ?>"></script>


<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      header: {
        left: 'prevYear,prev,next,nextYear today',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      },
      defaultDate: '<?php echo date("Y-m-d"); ?>',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        <?php foreach($tests  as $t){  $exp = explode(" ",$t->training_date); ?>    

        {
          title: '<?php echo ucwords($t->title);?>',
          url: '#',
          start: '<?php echo $exp[0]; ?>'
        },
        
        <?php } ?>  
  
      ]
    });

    calendar.render();
  });

</script>
<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>

<?php
    
    $filter = "user_id = ".$this->session->userdata('user_id');
    $number_of_courses = $this->crud_model->get_all_courses_total($filter);
    

    $std_filter = "role_id =3 and org_id = ".$this->session->userdata('org');
    $number_of_students = $this->user_model->get_totl_users($std_filter);

    $course_filter = "c.user_id = ".$this->session->userdata('user_id')." and co.org_id = ".$this->session->userdata('org');
    $number_of_organizations = $this->crud_model->get_totl_number_of_courses($course_filter);

    //print_r($number_of_instructors);
?>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('schedule-Trainings'); ?>
                 
                </h4>
<div class="row">
    <div class="col-12">
        <div class="card widget-inline">
            <div class="card-body p-0">
                <div class="row no-gutters">
                    <div class="col-sm-6 col-xl-3">
                        <a href="<?php echo site_url('admin/courses'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0">
                                <div class="card-body text-center">
                                    <i class="dripicons-archive text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_courses->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_courses'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6 col-xl-3">
                        <a href="<?php echo site_url('admin/courses'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <i class="dripicons-camcorder text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_organizations->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_organizations'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                   

                    <div class="col-sm-6 col-xl-3">
                        <a href="<?php echo site_url('admin/users'); ?>" class="text-secondary">
                            <div class="card shadow-none m-0 border-left">
                                <div class="card-body text-center">
                                    <i class="dripicons-user-group text-muted" style="font-size: 24px;"></i>
                                    <h3><span><?php echo $number_of_students->total; ?></span></h3>
                                    <p class="text-muted font-15 mb-0"><?php echo get_phrase('number_of_student'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>

                </div> <!-- end row -->
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end col-->
</div>
                <div class="row">
                    <div class="col-xl-12">
                

                           
                            <div id="basicwizard">
                                

                                <div class="tab-content b-0 mb-0">
                                   
                                    <div id='calendar'></div>

                       
                                </div> <!-- tab-content -->
                            </div> <!-- end #progressbarwizard-->
        </div>
    </div><!-- end row-->
</div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('user/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function GetOptions(val){

    var data ="";
    for(a=1; a<=val; a++){

        data+='<label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer');?>'+a+'<span class="required">*</span></label><div class="col-md-7"><input type="text" class="form-control" id="ans'+a+'" name = "ans'+a+'" placeholder="<?php echo get_phrase('enter_anwer'); ?>" required></div><div class="col-md-3"><input type="checkbox" name="opt'+a+'" class=""><div class="row">&nbsp;</div></div>';
    }

    document.getElementById("replace_me").innerHTML=data;
    document.getElementById("total").value=a-1;

}


function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
