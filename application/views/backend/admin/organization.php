<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                <a href = "<?php echo site_url('admin/add_organization/add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_organization'); ?></a>
                


            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('organizations'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('organization_name'); ?></th>
                      <th><?php echo get_phrase('domain_name'); ?></th>
                      <th><?php echo get_phrase('assign_courses').' / '.get_phrase('trainer'); ?></th>
                      <th><?php echo get_phrase('created_on'); ?></th>
                      <th><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($organizations as $key => $org): 
                        $courses = $this->crud_model->get_assign_courses_organization($org->org_id);

                        ?>
                        <tr>
                            <td><?php echo $key+1; ?></td>
                            <td><?php echo ucwords($org->name); ?></td>
                            <td><?php echo ($org->domain); ?></td>
                            <td style="background-color:#e6dae5 ;">
                          <?php foreach($courses as $c):
                            $enrol = $this->crud_model->get_total_enrolled_in_course_with_organization($c->id,$org->org_id);
                           // print_r($enrol);
                            $trainer = $this->user_model->get_course_instructor($c->id);
                            $trainer_name = $trainer->first_name." ".$trainer->last_name;


                                      echo "<b class='badge-success'>".strtoupper($c->title)."</b>
                                      &nbsp;/&nbsp; <b class='badge-success'>".strtoupper($trainer_name)."</b><br>";
                                      echo "<span style='color:blue;'>Total seats: <b>".$c->seats ."</b></span><span >&nbsp; Reserved: <b>".$enrol->total ."</b></span><span style='color:brown;'>&nbsp; Available: <b>".($c->seats-$enrol->total) ."</b></span><br>";
                                      echo "<span class='badge-danger' style='color:white;'>Training date: <b>".$c->training_date ."</b><hr/></span>";

                          endforeach; ?></td>
                            <td><?php echo date('Y-m-d', $org->created_on); ?></td>
                            <td>
                                <div class="dropright dropright">
 
                              <a href="<?php echo site_url('admin/ogadmins?og='.$org->org_id); ?>">
                                  <button type="button" class="btn btn-warning">Admin</button>
                              </a> 

                              <button type="button" class="btn btn-primary" onclick="assign_course('<?php echo $org->org_id; ?>');">Course-Assign</button>

                             <!--  <a href="<?php echo site_url('admin/instructors?og='.$org->org_id); ?>">
                                    <button type="button" class="btn btn-success">Trainers</button>
                              </a>

                              <a href="<?php echo site_url('admin/candidates?og='.$org->org_id); ?>">
                                  <button type="button" class="btn btn-primary">Candidates</button>
                              </a>  -->
                                  &nbsp;
                                  <button type="button" class="btn btn-sm btn-outline-danger btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="mdi mdi-dots-vertical"></i>
                                  </button>
 
                                  <ul class="dropdown-menu">
                                      
                                      <!-- <li><a class="dropdown-item" href="<?php echo site_url('admin/edit_organization/'.$org->org_id) ?>"><?php echo get_phrase('edit'); ?></a></li> -->
                                      <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/delete_organization/'.$org->org_id); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                  </ul>
                            </div>
                            </td>
                        </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
