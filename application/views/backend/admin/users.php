<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                <a href = "<?php echo site_url('admin/user_form/add_candidate_form/'.$org_id); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_candidate'); ?></a>
            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('candidates'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('photo'); ?></th>
                      <th><?php echo get_phrase('name'); ?></th>
                      <th><?php echo get_phrase('email'); ?></th>
                      <th><?php echo get_phrase('phone'); ?></th>
                      <th><?php echo get_phrase('Company'); ?></th>
                      <th><?php echo get_phrase('domain'); ?></th>
                      <th><?php echo get_phrase('enrolled&nbsp;trainings'); ?></th>
                      <th><?php echo get_phrase('status'); ?></th>
                      <th><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $key = 0; //print_r($users);
                       foreach ($users as  $user): $key++; 

                        $filter_org = 'org_id = '.$user->org_id;
                        $orgnaization = $this->crud_model->get_organizations($filter_org);
                        $domain = $this->crud_model->get_all_courses($user->domain_course_id);
                        
                        ?>
                          <tr>
                              <td><?php echo $key; ?></td>
                              <td>
                                  <img src="<?php echo $this->user_model->get_user_image_url($user->id);?>" alt="" height="50" width="50" class="img-fluid rounded-circle img-thumbnail">
                              </td>
                              <td><?php echo $user->first_name.' '.$user->last_name; ?>
                                <?php if($user->status != 1): ?>
                                  <small><p><?php echo get_phrase('status'); ?>: <span class="badge badge-danger-lighten"><?php echo get_phrase('unverified'); ?></span></p></small>
                                <?php endif; ?>
                              </td>
                              <td><?php echo $user->email; ?></td>
                              <td><?php echo $user->phone; ?></td>
                              <td><?php echo ucwords($orgnaization[0]->name); ?></td>
                              <td><?php echo ($orgnaization[0]->domain);?></td>
                              <td>
                                 <?php
                                    $enrolled_trainings = $this->user_model->get_course_members($user->id);


                                    $enrolled_courses = $this->crud_model->enrol_history_by_user_id($user->id);?>
                                    <ul>
                                        <?php foreach ($enrolled_trainings as $training):?>
                                            <li><?php echo $training->course_name; ?> <a href="<?php echo site_url('admin/remove_enrolled_from_course/'.$user->id) ?>"><i class="dripicons-trash"></i></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                              </td>

                              <td>
                                  <?php 
                                      if($user->status == 1){
                                  ?>      
                                        <span class="badge badge-success-lighten">active</span>
                                  <?php
                                      }else{
                                  ?>      
                                        <span class="badge badge-danger-lighten">In active</span>

                                  <?php
                                      }


                                  ?>
                              </td>  

                              <td>
                                  <div class="dropright dropright">
                                    <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </button>
                                    <ul class="dropdown-menu">

                                      <?php if($user->status == 0):?>
                                        <li><a class="dropdown-item" href="<?php echo site_url('admin/verify_account/'.$user->id) ?>"><?php echo get_phrase('active'); ?></a></li>
                                      <?php endif; ?>  
                                      
                                      <?php if($user->status == 1):?>
                                       <li><a class="dropdown-item" href="#" onclick="assign_course_to_student('<?php echo $user->id; ?>');"><?php echo get_phrase('assign_course'); ?></a></li>
                                      <?php endif; ?>  

                                        <li><a class="dropdown-item" href="<?php echo site_url('admin/user_form/edit_user_form/'.$user->id) ?>"><?php echo get_phrase('edit'); ?></a></li>

                                        <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/users/delete/'.$user->id); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                    </ul>
                                </div>
                              </td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
