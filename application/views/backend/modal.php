<script type="text/javascript">
function showAjaxModal(url, header)
{
    // SHOWING AJAX PRELOADER IMAGE
    jQuery('#scrollable-modal .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="<?php echo base_url().'assets/global/bg-pattern-light.svg'; ?>" /></div>');
    jQuery('#scrollable-modal .modal-title').html('...');
    // LOADING THE AJAX MODAL
    jQuery('#scrollable-modal').modal('show', {backdrop: 'true'});

    // SHOW AJAX RESPONSE ON REQUEST SUCCESS
    $.ajax({
        url: url,
        success: function(response)
        {
            jQuery('#scrollable-modal .modal-body').html(response);
            jQuery('#scrollable-modal .modal-title').html(header);
        }
    });
}
function showLargeModal(url, header)
{
    // SHOWING AJAX PRELOADER IMAGE
    jQuery('#large-modal .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="<?php echo base_url().'assets/global/bg-pattern-light.svg'; ?>" height = "50px" /></div>');
    jQuery('#large-modal .modal-title').html('...');
    // LOADING THE AJAX MODAL
    jQuery('#large-modal').modal('show', {backdrop: 'true'});

    // SHOW AJAX RESPONSE ON REQUEST SUCCESS
    $.ajax({
        url: url,
        success: function(response)
        {
            jQuery('#large-modal .modal-body').html(response);
            jQuery('#large-modal .modal-title').html(header);
        }
    });
}
</script>

<!-- (Large Modal)-->
<div class="modal fade" id="large-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                ...
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- Scrollable modal -->
<div class="modal fade" id="scrollable-modal" tabindex="-1" role="dialog" aria-labelledby="scrollableModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="scrollableModalTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body ml-2 mr-2">

        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal"><?php echo get_phrase("close"); ?></button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
function confirm_modal(delete_url)
{
    jQuery('#alert-modal').modal('show', {backdrop: 'static'});
    document.getElementById('update_link').setAttribute('href' , delete_url);
}

function assign_course_to_student(user_id)
{ 
    jQuery('#assign-course-to-student').modal('show', {backdrop: 'static'});
    document.getElementById('user_id').value=user_id;
}
function og_assign_course_to_student(user_id)
{ 
    jQuery('#og-assign-course-to-student').modal('show', {backdrop: 'static'});
    document.getElementById('og_user_id').value=user_id;
}
function confirm_reject_course(course_id)
{
    jQuery('#assign-course-modal').modal('show', {backdrop: 'static'});
    document.getElementById('cid').value=course_id;
}
function assign_course(org_id)
{
    jQuery('#assign-course-organization').modal('show', {backdrop: 'static'});
    document.getElementById('org_id').value=org_id;
}
function reset_password(reset_user_id)
{
    jQuery('#reset-password').modal('show', {backdrop: 'static'});
    document.getElementById('reset_user_id').value=reset_user_id;
}
</script>

<!-- Info Alert Modal -->
<div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center">
                    <i class="dripicons-information h1 text-info"></i>
                    <h4 class="mt-2"><?php echo get_phrase("heads_up"); ?>!</h4>
                    <p class="mt-3"><?php echo get_phrase("are_you_sure"); ?>?</p>
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <a href="#" id="update_link" class="btn btn-danger my-2"><?php echo get_phrase("continue"); ?></a>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Info Alert Modal -->
<div id="reset-password" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center">
                    <form action="<?php echo site_url('/OgAdmin/reset_password'); ?>" method="post">
                        <input type="hidden" name="reset_user_id" id="reset_user_id">
                    <i class="dripicons-information h1 text-info"></i>
                    <h4 class="mt-2"><?php echo get_phrase("Reset Password"); ?>!</h4>
                    <p class="mt-3"><input type="text" name="reset_password" class="form-control"></p>
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <button class="btn btn-danger my-2" type="submit"><?php echo get_phrase("continue"); ?></button>
                   </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- assign new course  -->
<div id="assign-course-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center">
                    <h4 class="mt-2"><?php echo get_phrase("rejection_remarks"); ?>!</h4>

                    <form action="<?php echo site_url('/admin/remarks'); ?>" method="post">
                    <p> <textarea required name="remarks" class="form-control" placeholder="Reason"></textarea><input type="hidden" id="cid" name="cid">
</p>
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <button type="submit" class="btn btn-danger my-2"><?php echo get_phrase("submit"); ?></button>
                </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="assign-course-organization" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"  style="width:600px;">
            <div class="modal-body p-4">
                <div class="text-center">
                    <form action="<?php echo site_url('/admin/assign_course_org'); ?>" method="post">

                    <h4 class="mt-2"><?php echo get_phrase("assign-course"); ?>!</h4>
                    <input type="hidden" name="org_id" id="org_id">
                <table cellpadding="10" cellspacing="10">
                    <tr>
                     <td><b>Select course: </b></td>    
                    <td colspan="3">
                    <select name="course_id" class="form-control">
                    <?php 
                        $courses = $this->crud_model->get_all_courses();
                        foreach($courses as $c){
                         ?>
                        <option value="<?php echo $c->id;?>"><?php echo ucwords($c->title);?></option>

                    <?php   
                        }//extract foreach
                    ?>
                    </select>
                    </td>
                    </tr>
                    <tr>
                        <td><b>Training Date:</b></td>

                       <td> 
                        <input type="date" class="form-control" id="training_date" name="tr_date" placeholder="<?php echo get_phrase('training date time...'); ?>">
                       </td>

                       
                       <td> 
                        <select name="tr_time" class="form-control" placeholder="training-date" required>
                                                                        
                                                            <option value="00:00">00:00</option>
                                                            <option value="00:30">00:30</option>
                                                            <option value="01:00">01:00</option>
                                                            <option value="01:30">01:30</option>
                                                            <option value="02:00">02:00</option>
                                                            <option value="02:30">02:30</option>
                                                            <option value="03:00">03:00</option>
                                                            <option value="00:00">03:30</option>
                                                            <option value="03:30">04:00</option>
                                                            <option value="04:30">04:30</option>
                                                            <option value="05:00">05:00</option>
                                                            <option value="05:30">05:30</option>
                                                            <option value="06:00">06:00</option>
                                                            <option value="06:30">06:30</option>
                                                            <option value="07:00">07:00</option>
                                                            <option value="07:30">07:30</option>
                                                            <option value="08:00">08:00</option>
                                                            <option value="08:30">08:30</option>
                                                            <option value="09:00">09:00</option>
                                                            <option value="09:30">09:30</option>
                                                            <option value="10:00">10:00</option>
                                                            <option value="10:30">10:30</option>
                                                            <option value="11:00">11:00</option>
                                                            <option value="11:30">11:30</option>
                                                            <option value="12:00">12:00</option>
                                                            <option value="12:30">12:30</option>
                                                                        
                                                        </select>
                                                </td>
                                                <td>        

                                                        <select name="ampm" class="form-control" required>
                                                                <option value="AM">AM</option>
                                                                <option value="PM">PM</option>
                                                        </select>  

                                                </td>
                                             </tr>
                                             <tr>
                                                 <td><b>seats available</b></td> 
                                                 <td colspan="3"><input type="number" class="form-control" name="seats" id="seats" placeholder="seats..." required="true"></td>  
                                             </tr>   

                                        </table>        
                    
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <button type="submit" class="btn btn-danger my-2"><?php echo get_phrase("submit"); ?></button>
                </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="assign-course-to-student" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width:300px;">
            <div class="modal-body p-4">
                <div class="text-center">
                    <form action="<?php echo site_url('/admin/assign_course_to_student'); ?>" method="post">

                    <h4 class="mt-2"><?php echo get_phrase("assign-course"); ?>!</h4>
                    <input type="hidden" name="user_id" id="user_id">

                <table cellpadding="10" cellspacing="10">
                    <tr>
                    <td colspan="3">
                    <select name="course_id" class="form-control">
                        <option value="">--- select course ---</option>

                    <?php 

                        $courses = $this->crud_model->get_all_courses();

                        foreach($courses as $c){
                         ?>
                        <option value="<?php echo $c->id;?>"><?php echo ucwords($c->title);?></option>

                    <?php   
                        }//extract foreach
                    ?>
                    </select>
                    </td>
                    </tr>
                </table>        
                    
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <button type="submit" class="btn btn-danger my-2"><?php echo get_phrase("submit"); ?></button>
                </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="og-assign-course-to-student" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width:300px;">
            <div class="modal-body p-4">
                <div class="text-center">
                    <form action="<?php echo site_url('/OgAdmin/assign_course_to_student'); ?>" method="post">

                    <h4 class="mt-2"><?php echo get_phrase("organization-admin-assign-course"); ?>!</h4>
                    <input type="hidden" name="og_user_id" id="og_user_id">

                <table cellpadding="10" cellspacing="10">
                    <tr>
                    <td colspan="3">
                    <select name="course_id" class="form-control">
                        <option value="">--- select course ---</option>

                    <?php 

                        $courses = $this->crud_model->get_all_courses();

                        foreach($courses as $c){
                         ?>
                        <option value="<?php echo $c->id;?>"><?php echo ucwords($c->title);?></option>

                    <?php   
                        }//extract foreach
                    ?>
                    </select>
                    </td>
                    </tr>
                </table>        
                    
                    <button type="button" class="btn btn-info my-2" data-dismiss="modal"><?php echo get_phrase("cancel"); ?></button>
                    <button type="submit" class="btn btn-danger my-2"><?php echo get_phrase("submit"); ?></button>
                </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
