<link href="<?php echo base_url('assets/events/core/main.css'); ?>" rel='stylesheet' />
<link href="<?php echo base_url('assets/events/daygrid/main.css'); ?>" rel='stylesheet' />
<script src="<?php echo base_url('assets/events/core/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/interaction/main.js'); ?>"></script>
<script src="<?php echo base_url('assets/events/daygrid/main.js'); ?>"></script>
<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      header: {
        left: 'prevYear,prev,next,nextYear today',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      },
      defaultDate: '<?php echo date("Y-m-d"); ?>',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
         <?php foreach($tests  as $t){  $exp = explode(" ",$t->training_date); ?>    

        {
          title: '<?php echo ucwords($t->title);?>',
          url: '#',
          start: '<?php echo $exp[0]; ?>'
        },
        
        <?php } ?>  
  
      ]
    });

    calendar.render();
  });

</script>
<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>

<?php
$my_courses = $this->user_model->my_courses()->result_array();
?>
<?php include("include_navigation.php"); ?>


<section class="my-courses-area">
    <div class="container">                            
          <h4>Upcomming Trainings</h4>
        
    </div>
</section>
    

<div id='calendar'></div>


