<?php
        if($page_name == "my_courses"){ $pgtitle = "My Courses";  }
        else if($page_name == "user_profile" || $page_name == "user_credentials" ||  $page_name == "update_user_photo"){ $pgtitle = "Profile";}
        else if($page_name == "my_tests"){ $pgtitle = "Schedule Test";  }
        
?>

<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase($pgtitle); ?></h1>
                <ul>
                  <li <?php 
                       if($page_name == "my_courses"){ echo 'class="active"'; }?> >
                   <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('all_courses'); ?></a></li>

    
                  <li <?php 
                       if($page_name == "user_profile" || $page_name == "user_credentials" ||  $page_name == "update_user_photo"){ echo 'class="active"'; }?> >
                    <a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a>
                 </li>



                  <li <?php if($page_name == "my_tests"){ echo 'class="active"'; }?> >
                    <a href="<?php echo site_url('home/tests'); ?>"><?php echo get_phrase('schedule_tests'); ?></a>
                  </li>


                  <li <?php if($page_name == "events"){ echo 'class="active"'; }?> >
                    <a href="<?php echo site_url('home/events'); ?>"><?php echo get_phrase('training_calendar'); ?></a>
                  </li>

                </ul>
            </div>
        </div>
    </div>
</section>