<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>


<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('Result Test'); ?>
                
                    <a href="<?php echo site_url('user/get_result/'.$identity.'/'.$student_id); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm ml-1" ><?php echo get_phrase('Summary Result'); ?>  </a>&nbsp;
                    <a href="<?php echo site_url('user/test_details/'.$identity.'/'.$student_id); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"> <?php echo get_phrase('Test Details'); ?> </a>&nbsp;
                   
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                    
        
        
        <?php
            $q=0; $answers =0;
            foreach($questions as $QS){ $q++;

                $answers += $this->crud_model->get_student_answer_points($QS->id,$student_id); 
                                           
             }//foreach 

           
            ?>

                    <table class="table" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th><label>Name :</label> <?php echo ucwords($user_test[0]->first_name." ".$user_test[0]->last_name) ; ?></th>
                                <th><label>Test Title:</label> <?php echo ucwords($user_test[0]->test_title) ; ?></th>
                                <th><label>Course    :</label> <?php echo ucwords($user_test[0]->course_name) ; ?></th>
                               
                            </tr>
                            <tr style="background-color:#dee2e6;">
                                <th><label>Total Questions:</label> <?php echo sizeof($total_questions) ; ?></th>
                                <th><label>Right Answers:</label> <?php echo $answers; ?></th>
                                <th><label>Wrong Answers:</label> <?php echo (sizeof($total_questions)-$answers) ; ?></th>
                                <?php
                                    $percentage = (($answers/sizeof($total_questions))*100);

                                    if($percentage >= 50 ){

                                        $result_status = "Pass";
                                    }else{

                                        $result_status = "Fail";
                                    }

                                ?>
                            </tr>
                            <tr>
                                <th>Result      : </th>
                                <th> 
                                <button class="btn btn-success"><?php echo $percentage.'%' ; ?></button>
                                </th>

                                <th>
                                <button class="btn btn-danger"><?php echo strtoupper($result_status) ; ?></button>
                                </th>    
                                
                               
                            </tr>
                        </thead>
                    </table>
                        

                            

                <?php //endif; ?>
                




        
                    </div>
                </div><!-- end row-->
</div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('user/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function GetOptions(val){

    var data ="";
    for(a=1; a<=val; a++){

        data+='<label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer');?>'+a+'<span class="required">*</span></label><div class="col-md-7"><input type="text" class="form-control" id="ans'+a+'" name = "ans'+a+'" placeholder="<?php echo get_phrase('enter_anwer'); ?>" required></div><div class="col-md-3"><input type="checkbox" name="opt'+a+'" class=""><div class="row">&nbsp;</div></div>';
    }

    document.getElementById("replace_me").innerHTML=data;
    document.getElementById("total").value=a-1;

}


function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
