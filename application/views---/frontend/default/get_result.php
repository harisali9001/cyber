<?php

$my_courses = $this->user_model->my_courses()->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
    if (!in_array($course_details['category_id'], $categories)) {
        array_push($categories, $course_details['category_id']);
    }
}
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase('Result'); ?></h1>
                <ul>
                  <li><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('all_courses'); ?></a></li>
<!--                   <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo get_phrase('wishlists'); ?></a></li>
 -->                  <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li>
<!--                   <li><a href="<?php echo site_url('home/purchase_history'); ?>"><?php echo get_phrase('purchase_history'); ?></a></li>
 -->                  <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li>
                      <li class="active"><a href="<?php echo site_url('home/tests'); ?>"><?php echo get_phrase('schedule_tests'); ?></a></li>

                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">                            
     
 <a href="<?php echo site_url('home/get_result/'.$identity); ?>" style="background-color: brown;" class="btn btn-rounded" ><?php echo ('Summary Result'); ?>  </a>&nbsp;
 <a href="<?php echo site_url('home/test_details/'.$identity); ?>" class="btn btn-secondary"> <?php echo ('Test Details'); ?> </a>&nbsp;
                   
        
        
        <div class="table-responsive-sm mt-4">

        <?php
            $q=0; $answers =0;
            foreach($questions as $QS){ $q++;

                $answers += $this->crud_model->get_answer_points($QS->id); 
                                           
             }//foreach 

           
            ?>


               
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th><label>Name :</label> <?php echo ucwords($user_test[0]->first_name." ".$user_test[0]->last_name) ; ?></th>
                                <th><label>Test Title:</label> <?php echo ucwords($user_test[0]->test_title) ; ?></th>
                                <th><label>Course    :</label> <?php echo ucwords($user_test[0]->course_name) ; ?></th>
                               
                            </tr>
                            <tr style="background-color:#dee2e6;">
                                <th><label>Total Questions:</label> <?php echo sizeof($total_questions) ; ?></th>
                                <th><label>Right Answers:</label> <?php echo $answers; ?></th>
                                <th><label>Wrong Answers:</label> <?php echo (sizeof($total_questions)-$answers) ; ?></th>
                                <?php
                                    $percentage = (($answers/sizeof($total_questions))*100);

                                    if($percentage >= 50 ){

                                        $result_status = "Pass";
                                    }else{

                                        $result_status = "Fail";
                                    }

                                ?>
                            </tr>
                            <tr>
                                <th>Result      : </th>
                                <th> 
                                <button class="btn" style="background-color: #3ebf3e;width:300px;"><?php echo $percentage.'%' ; ?></button>
                                </th>

                                <th>
                                <button class="btn" style="background-color: #727cf5;width:300px;"><?php echo strtoupper($result_status) ; ?></button>
                                </th>    
                                
                               
                            </tr>
                        </thead>
                        <tbody>

                            

                            <?php $key=0;

                             foreach ($tests as  $T):
                                
                                $test_date = empty($T->test_date)?"---":date('d/M/Y', $T->test_date);
                            ?>
                                <tr>
                                    <td><?php echo ++$key; ?></td>
                                    <td>
                                        <strong><a href="#"><?php echo $T->test_title; ?></a></strong>                                       
                                    </td>
                                    <td>
                                        <strong><?php echo ellipsis($T->course_name); ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo ellipsis($T->first_name.' '.$T->last_name); ?></strong>
                                    </td>

                                    <td><?php echo date('d/M/Y', $T->added_on) ?></td>
                                    <td><?php echo $test_date; ?></td>

                                    <td>
                                        
                                        <?php 
                                           if($T->test_done == 0){ 
                                        ?>
                                        <a class="btn" style="background-color:green;" href="<?php echo site_url('home/start_test/'.$T->identity); ?>"><?php echo ucwords('Start Test'); ?></a>
                                        <?php }
                                        ?>  
                                        <a class="btn" href="<?php echo site_url('home/get_result/'.$T->identity); ?>"><?php echo ucwords('Result Test'); ?></a>  
                                    </td>
                                    
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php //endif; ?>
                
            </div>




        
    </div>
    </section>
    


<script type="text/javascript">
function getCoursesByCategoryId(category_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_category'); ?>',
        data : {category_id : category_id},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCoursesBySearchString(search_string) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_search_string'); ?>',
        data : {search_string : search_string},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCourseDetailsForRatingModal(course_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/get_course_details'); ?>',
        data : {course_id : course_id},
        success : function(response){
            $('#course_title_1').append(response);
            $('#course_title_2').append(response);
            $('#course_thumbnail_1').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_thumbnail_2').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_id_for_rating').val(course_id);
            // $('#instructor_details').text(course_id);
            console.log(response);
        }
    });
}
</script>
