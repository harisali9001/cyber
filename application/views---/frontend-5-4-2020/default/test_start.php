<?php

$my_courses = $this->user_model->my_courses()->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
    if (!in_array($course_details['category_id'], $categories)) {
        array_push($categories, $course_details['category_id']);
    }
}
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase('my_courses'); ?></h1>
                <ul>
                  <li class="active"><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('all_courses'); ?></a></li>
<!--                   <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo get_phrase('wishlists'); ?></a></li>
 -->                  <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li>
<!--                   <li><a href="<?php echo site_url('home/purchase_history'); ?>"><?php echo get_phrase('purchase_history'); ?></a></li>
 -->                  <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li>
                      <li><a href="<?php echo site_url('home/tests'); ?>"><?php echo get_phrase('schedule_tests'); ?></a></li>

                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">                            
     
            <form action= "/home/save_test" method="post" name="start_test_name">

        <?php
            $q=0;
            foreach($questions as $QS){ $q++;
        ?>
            <input type="hidden" name="question[]" value="<?php echo $QS->id; ?>">
            <input type="hidden" name="identity" value="<?php echo $QS->identity; ?>">
        <div class="table-responsive-sm mt-4">
              
                <?php //if (count($courses) > 0): ?>
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th style="color:#727cf5;">Q.<?php echo $q; ?>:   <b><?php echo ucfirst($QS->question);?></b></th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                 <td>
                            

                                     <table>
                                         <tr>
                                          <?php $options = $this->crud_model->get_question_options($QS->id); ?>
        
                                    <?php $cnt=0 ;
                                            foreach($options as $OP){  $cnt++;

                                                    $bgcolor = $cnt%2==0?"#f7f8fa":"#dedfe1";  ?>       
                                                              
                                                        <td style="background-color:<?php echo $bgcolor;?>">
                                                            <input type="checkbox" name="opt[<?php echo $QS->id; ?>][<?php echo $cnt;?>]" value="<?php echo $OP->op_id;?>"> <b><?php echo ucwords($OP->options);?></b></td>
                                                        
                                                   

                                           <?php }//options foreach ?>  
                                            </tr>    
                                             
                                     </table>         
                                </td>   
                            </tr>    

                            

                            
                        </tbody>
                    </table>


        <?php
            }//foreach questions
        ?>
                      <button type="submit" class="btn btn-success" style="background-color:green;">Submit</button>
                      <button type="button" class="btn btn-success">Cancel</button>
                      

              
                
            </div>
  </form>



        
    </div>
    </section>
    


<script type="text/javascript">
function getCoursesByCategoryId(category_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_category'); ?>',
        data : {category_id : category_id},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCoursesBySearchString(search_string) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_search_string'); ?>',
        data : {search_string : search_string},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCourseDetailsForRatingModal(course_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/get_course_details'); ?>',
        data : {course_id : course_id},
        success : function(response){
            $('#course_title_1').append(response);
            $('#course_title_2').append(response);
            $('#course_thumbnail_1').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_thumbnail_2').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_id_for_rating').val(course_id);
            // $('#instructor_details').text(course_id);
            console.log(response);
        }
    });
}
</script>
