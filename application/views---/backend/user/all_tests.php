<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('Manage Tests').': '.$course_details['title']; ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3"><?php echo get_phrase('Test Manager'); ?>
                    <a href="<?php echo site_url('user/all_tests/'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm ml-1" ><?php echo get_phrase('View All Tests'); ?> <i class="mdi mdi-arrow-right"></i> </a>
                    <a href="<?php echo site_url('user/tests'); ?>" class="alignToTitle btn btn-outline-secondary btn-rounded btn-sm"><i class=" mdi mdi-keyboard-backspace"></i> <?php echo get_phrase('create test'); ?></a>
                </h4>

                <div class="row">
                    <div class="col-xl-12">
                        <form class="required-form" action="<?php echo site_url('user/save_test/save/'.$course_id); ?>" method="post" enctype="multipart/form-data">
 
                            <input type="hidden" name="total" id="total">    
                            <input type="hidden" name="identity" id="identity" value="<?php echo $test_details->identity; ?>">    
                            <div id="basicwizard">
                                

                                <div class="tab-content b-0 mb-0">
                                   

                                
            <div class="table-responsive-sm mt-4">
                <?php if (count($tests) > 0): ?>
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='25'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo get_phrase('Title'); ?></th>
                                <th><?php echo get_phrase('Course'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $key=0;

                             foreach ($tests as  $T):
                              //  $options = $this->crud_model->get_all_options($T->id);
                              //  $correct_options = $this->crud_model->get_all_options($T->id,1);

                                
                            ?>
                                <tr>
                                    <td><?php echo ++$key; ?></td>
                                    <td>
                                        <strong><a href="#"><?php echo $T->title; ?></a></strong>                                       
                                    </td>
                                    <td>
                                        <strong><?php echo ellipsis($T->course_name); ?></strong>
                                    </td>
                                 
                                    <td>
                                        <a class="btn btn-danger" href="#" onclick="confirm_modal('<?php echo site_url('user/delete_all_test/'.$T->identity); ?>');"><?php echo get_phrase('delete'); ?></a>
                                        <a class="btn btn-primary" href="<?php echo site_url('user/edit_test/'.$T->identity); ?>"><?php echo get_phrase('edit'); ?></a>  
                                        <a class="btn btn-success" href="<?php echo site_url('user/assign_test/'.$T->identity.'/'.$T->course_id); ?>"><?php echo get_phrase('assign Test'); ?></a>  
                                        <a class="btn btn-warning" href="<?php echo site_url('user/schedule_tests/'.$T->identity.'/'.$T->course_id); ?>"><?php echo get_phrase('Schedule Test'); ?></a>  
                                    </td>
                                    
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php if (count($tests) == 0): ?>
                    <div class="img-fluid w-100 text-center">
                      <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                      <?php echo get_phrase('no_data_found'); ?>
                    </div>
                <?php endif; ?>
            </div>
        

                       
                    </div> <!-- tab-content -->
                </div> <!-- end #progressbarwizard-->
            </form>
        </div>
    </div><!-- end row-->
</div> <!-- end card-body-->
</div> <!-- end card-->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
    togglePriceFields('is_free_course');
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('user/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function GetOptions(val){

    var data ="";
    for(a=1; a<=val; a++){

        data+='<label class="col-md-2 col-form-label" for="course_title"><?php echo get_phrase('Answer');?>'+a+'<span class="required">*</span></label><div class="col-md-7"><input type="text" class="form-control" id="ans'+a+'" name = "ans'+a+'" placeholder="<?php echo get_phrase('enter_anwer'); ?>" required></div><div class="col-md-3"><input type="checkbox" name="opt'+a+'" class=""><div class="row">&nbsp;</div></div>';
    }

    document.getElementById("replace_me").innerHTML=data;
    document.getElementById("total").value=a-1;

}


function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});
</script>
