<?php 
	include_once("class.phpmailer.php");

/*error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if ( ! function_exists('get_random_password'))
{
    /**
     * Generate a random password. 
     * 
     * get_random_password() will return a random password with length 6-8 of lowercase letters only.
     *
     * @access    public
     * @param    $chars_min the minimum length of password (optional, default 6)
     * @param    $chars_max the maximum length of password (optional, default 8)
     * @param    $use_upper_case boolean use upper case for letters, means stronger password (optional, default false)
     * @param    $include_numbers boolean include numbers, means stronger password (optional, default false)
     * @param    $include_special_chars include special characters, means stronger password (optional, default false)
     *
     * @return    string containing a random password 
     */    
    function get_random_password($chars_min=6, $chars_max=12, $use_upper_case=true, $include_numbers=true, $include_special_chars=true)
    {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
             $selection .= "!^%~&@#";
        }
                                
        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                
        
      return $password;
    }

}


	function validation($ms_id,$role_id,$user_id)
	{
		$ci =& get_instance();
		$ci->load->database();
	
		$sql= "select * from  ".__TABLE_MANUSCRIPT__." as m
		       			inner join ".__TABLE_USER_ROLE__." as usr on usr.journal_id = m.journal_id
		       			where m.manuscript_id='".$ms_id."' and usr.role_id='".$role_id."' and usr.user_id='".$user_id."' ";      
		$query = $ci->db->query($sql);
		if($query->num_rows() > 0) 
		{
			return true;
		}
		else
		{
			redirect('/Login/getRedirectionPage');
		}
	}


if ( ! function_exists('custom_encrypt'))
	{
		function custom_encrypt() {			
			$num_args = func_num_args();	
			$param= "";
			for($x=0;$x<$num_args;$x++) {				
				$param .= trim(func_get_arg($x))."||";			
			}			
			$param = substr($param,0,strlen($param)-2);			
			$param = base64_encode($param);
			$m=1;
			$str="";			
			for($i=0;$i<strlen($param);$i++) {
				$str .=substr($param,$i,1);				
				if($m==4) {					
					$possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
					$val = substr($possible_charactors, rand(0,strlen($possible_charactors))%(strlen($possible_charactors)-20),1);
					$str .=$val;
					$m=1;
				}				
			$m++;
		}
		$str = str_replace("=","TcVY",$str);
		return $str;
		} //function 
}
if ( ! function_exists('custom_decrypt')){
	function custom_decrypt($str) {
			$arr=array('%','$','/','\\','*','+','-','\'','"','#','@','(',')','^','~','`','&',

					'_',',',':',';','?','<','>','!','{','}','[',']');
			$str=str_replace($arr,'',$str);
			$str = str_replace("TcVY","=",$str);
			$m=1;
			$d_str="";
			for($i=0;$i<strlen($str);$i++) {
				if($m!=5) {
						$d_str .= substr($str,$i,1);
					} else {
						$m=1;
					}
				$m++;		
			}
			$d_str = base64_decode($d_str);
			return explode("||",$d_str);
	} //function
}

if ( ! function_exists('get_user_details'))
{
   function get_user_details($user_id)
   {
       //get main CodeIgniter object
       $ci =& get_instance();
       //load databse library
       $ci->load->database();
       
       //get data from database
       //$query = $ci->db->get_where(__TABLE_USERS__,array('user_id'=>$user_id));
       $sql= "select u.user_id, u.password, u.email, u.created_on, u.created_by, u.updated_on, u.updated_by, u.ip, u.status, u.last_login, u.last_login_time,p.pr_id, p.suffix, p.first_name, p.last_name, p.h_index, p.address1, p.address2, p.address3, p.address4, p.country_id, p.keywords, p.field_of_expertise, p.signature, p.user_image,af.user_affiliation_id, af.institution, af.department, af.address, af.country, af.city, af.postal_code, af.phone, af.fax from  ".__TABLE_USERS__." as u 
       		inner join ".__TABLE_USERS_PROFILE__." as p on u.user_id = p.user_id
			left join ".__TABLE_USERS_AFF__." as af on u.user_id = af.user_id
       		where u.user_id='".$user_id."'";        
     
       $query = $ci->db->query($sql);    
       if($query->num_rows() > 0){
           $result = $query->row_array();
           return $result;
       }else{
           return false;
       }
   }
}
if ( ! function_exists('get_user_details2'))
{
   function get_user_details2($user_id)
   {
       //get main CodeIgniter object
       $ci =& get_instance();
       //load databse library
       $ci->load->database();
       
       //get data from database
       //$query = $ci->db->get_where(__TABLE_USERS__,array('user_id'=>$user_id));
       $sql= "select * from  ".__TABLE_USERS__." as u 
       		inner join ".__TABLE_USERS_PROFILE__." as p on u.user_id = p.user_id			
       		where u.user_id='".$user_id."'";      
     
       $query = $ci->db->query($sql);    
       if($query->num_rows() > 0){
           $result = $query->row_array();
           return $result;
       }else{
           return false;
       }
   }
}

if ( ! function_exists('getManuscriptFolderPath'))
{ 
	function getManuscriptFolderPath($manuscript_id,$folderType = '')
	{
	    $ci =& get_instance();
	    $ci->load->database();
	    $ci->db->from(__TABLE_MANUSCRIPT__.' as m');
	    $ci->db->join(__TABLE_JOURNALS__.' as j', 'm.journal_id = j.journal_id', 'inner');

	 	$ci->db->where('m.manuscript_id',$manuscript_id );	
	 	//echo $ci->db->get_compiled_select(); die; 	
	    $journal = $ci->db->get()->row();	    
	    $path = __ROOT_PATH__.$journal->journal_path.'articles/'.$manuscript_id.'/'.$folderType;
	    return $path;
  }
	   

}

if ( ! function_exists('moveFileLocation'))
{ 
	function moveFileLocation($ms_file_id){
	
		$ci =& get_instance();
	    $ci->load->database();
	    $ci->db->from(__TABLE_MANUSCRIPT_FILES__);	    
	    $ci->db->where('manuscript_file_id',$ms_file_id);
	    $file = $ci->db->get()->row();
	    
	    $ci =& get_instance();
	    $ci->load->database();
	    $ci->db->from(__TABLE_FILE_TYPE_NAMES__);	    
	    $fileTypes = $ci->db->get()->result();

	    foreach($fileTypes as $type)
	    {
    		if ($type->file_type_id == $file->file_type)
    		{
    			$targetFolder = $type->folder_name;
    		}
	    }
	    
		$path = getManuscriptFolderPath($file->manuscript_id);
		
		$mov_file_name = $ms_file_id .'_'.$file->file_name;
	    $sourcePath = $path.'miscellaneous/' . $mov_file_name;
	    $targetPath = $path.$targetFolder.'/' .$mov_file_name;
		
	/*	print $sourcePath; 
		print "<br>";
		print $targetPath;
	*/	
	
	    
	    if (!file_exists($sourcePath))
	    {
	    	$ci =& get_instance();
		    $ci->load->database();
		    $data = array('status' => 'N');
		    $ci->db->where('manuscript_file_id',$ms_file_id);
		    $ci->db->update(__TABLE_MANUSCRIPT_FILES__,$data);
		    return false;
	    }
	    else
	    {   
		    if (!copy($sourcePath, $targetPath)) 
			{
			    return false;
			}
			else
			{
				unlink($sourcePath);
				return true;
			}
	    }
	   exit;
		
	}
	
}


if ( ! function_exists('get_user_details'))
{
  function getManuscriptQCNoteFiles($qc_note_id)
  {    
    $ci =& get_instance();
    $ci->load->database();

    $ci->db->select('*')->from(__TABLE_QC_NOTE_FILES__.' as qcf');
    $ci->db->join(__TABLE_MANUSCRIPT_FILES__.' as f', 'qcf.file_id =f.manuscript_file_id', 'inner');
    $ci->db->where('qcf.qc_note_id',$qc_note_id );
    $files = $ci->db->result();    
    return $files;
    
   }


}

if ( ! function_exists('PaginationConfiguration')){ 
	function PaginationConfiguration($base_url,$per_page=10)
	{
		$config['base_url']		 	=	$base_url;
		$config['per_page'] 		=	$per_page;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] 	= 	"<ul class='pagination pagination-split'>";
		$config['full_tag_close'] 	=	"</ul>";
		$config['num_tag_open'] 	= 	'<li>';
		$config['num_tag_close'] 	= 	'</li>';
		$config['cur_tag_open'] 	= 	"<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] 	= 	"<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] 	= 	"<li>";
		$config['next_tag_close'] 	= 	"</li>";
		$config['prev_tag_open'] 	= 	"<li>";
		$config['prev_tag_close'] 	= 	"</li>";
		$config['first_tag_open'] 	= 	"<li>";
		$config['first_tag_close'] = 	"</li>";
		$config['last_tag_open'] 	= 	"<li>";
		$config['last_tag_close'] 	= 	"</li>";


		return $config;

	}

}

if ( ! function_exists('SendSmtpEmail')){ 

 function SendSmtpEmail($send_to,$from,$subject,$message_body,$name='',$manager_name='',$cc='0',$bcc='0'){
			
			//echo $message_body;
			$configs = Array(
							'protocol' => 'smtp',
							'smtp_host' => 'smtp.IPSoftSolution.com',
							'smtp_port' => 587,
							'smtp_user' => 'arsal@IPSoftSolution.com',
							'smtp_pass' => 'Arsal123',
				        	'wordwrap' => TRUE,
							'mailtype'  => 'html', 
            				'charset'   => 'iso-8859-1'
						);
						
				//$ci->load->library('email', $configs);
				
				$ci =& get_instance();
			   //load databse library
			    $ci->load->library('email',$configs);
			   //$ci->email->set_header('MIME-Version', '1.0; charset=utf-8');
			   //$ci->email->set_header('Content-type', 'text/html');
			   	//$ci->load->email($configs);
			
			$ci->email->set_newline("\r\n");
		
			$ci->email->from('admin@IPSoftsolution.com', $name);
				
			foreach($send_to as $to){
						
			$ci->email->to($to);

			}//to



			foreach($cc as $c){
						
				$ci->email->cc($c); 

			}//cc

				$ci->email->subject($subject);
				
				$ci->email->message($message_body);
					
				if($ci->config->config['send_email_flag']){	
					$result = $ci->email->send();
				}
				echo $ci->email->print_debugger();
				if($result == 0){echo "Email Not Send...";}else{
				echo "Email Send...";
				}

	
	}	
}//SendEmail



if ( ! function_exists('SendSmtpMail')){ 

       function SendSmtpMail($to,$from,$subject,$message,$receiver_name='',$manager_name='',$cc='0',$bcc='0',$attachment='') {


            $mail = new PHPMailer(); 
			$mail -> charSet = "UTF-8"; 
			$mail->IsSMTP(); // send via SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication     
            $mail->SMTPAutoTLS = true;

	
 			   
			$mail->Host = "smtp.gmail.com";  // Specify main and backup server
			$mail->SMTPSecure = "ssl";                         // Enable SMTP authentication
			$mail->Username ="IPSoftSolution.andi@gmail.com";                            // SMTP username
			$mail->Password = "IPSoftSolution123_"; 
			$mail->Port = 465;


			/******************************************************************************/
					/***/////    FROM  /////////////****/
			/******************************************************************************/
			$mail->AddReplyTo($from, $manager_name); //TO
	
			
			$mail->ReturnPath = "admin@IPSoftsolution.com"; //setting return path
			$mail->Sender = "admin@IPSoftsolution.com"; //setting return path						
			
			$mail->MessageID = "<" . md5('HELLO'.(idate("U")-1000000000).uniqid()).'-8'.'@IPSoftsolution.com>';
			$mail->XMailer = 'Microsoft Outlook 16.0';

			$mail->From = $from; //$manager_email;
			$mail->FromName = $manager_name; //$manager_name;

			/******************************************************************************/
					/***/////    END FROM  /////////////****/
			/******************************************************************************/

			/******************************************************************************/
					/***/////    SEND TO  /////////////****/
			/******************************************************************************/
			$to_emails = explode(",",$to);
			for($i=0;$i<count($to_emails); $i++) {
		        //$mail->AddAddress(trim($to_emails[$i]), $receiver_name);
				$mail->AddAddress(trim($to_emails[$i]));
			}
			/******************************************************************************/
					/***/////    END SEND TO  /////////////****/
			/******************************************************************************/


			/******************************************************************************/
					/***/////    CC  /////////////****/
			/******************************************************************************/
			$carr = explode(",",$cc);
			for($i=0;$i<count($carr); $i++) {
				$mail->AddCC($carr[$i]);
			}
			/******************************************************************************/
					/***/////  END CC /////////////****/
			/******************************************************************************/

			

			/******************************************************************************/
					/***/////    BCC  /////////////****/
			/******************************************************************************/
			$bcarr = explode(",",$bcc);
			for($i=0;$i<count($bcarr); $i++) {
				$mail->AddBCC($bcarr[$i]);
			}
			//$mail->AddBCC("manager@IPSoftsolution.net");
			//$mail->AddBCC("arshad@IPSoftSolution.com");
			/******************************************************************************/
					/***/////    END BCC  /////////////****/
			/******************************************************************************/



	
			$mail->WordWrap = 50;      // Set word wrap to 50 characters
			$mail->IsHTML(true);                               // Set mailer to use SMTP

			/******************************************************************************/
					/***/////    SUBJECT,MESSAGE,BODY  /////////////****/
			/******************************************************************************/
			$mail->Subject = $subject;
	
			if(!empty($attachment)){
				$message .= "<br/><br/><b>Attachments:</b><br/>".$attachment;				
				$mail->Body    = $message;
			}else{
			
				$mail->Body    = $message;
			
			}
			
			$mail->AltBody = $message;
			/******************************************************************************/
					/***/////    END SUBJECT,MESSAGE,BODY  /////////////****/
			/******************************************************************************/


			


			$CI =& get_instance();								
			if($CI->config->config['send_email_flag']){

				if(!$mail->Send()) {
					
				   echo 'Message could not be sent.';

				   echo 'Mailer Error: ' . $mail->ErrorInfo;

				   return false;

				}//Send()
				
				return true;

			}


		} //function

}


if ( ! function_exists('SendMailToDeveloper')){ 

       function SendMailToDeveloper($bug_fix_msg='') {

		   $subject  ="IPSoftsolution BUG FIXING";	
	       $message  =" Dear All <br>";
		   
		   $message .="<p>";
		   foreach($bug_fix_msg as $caption=>$value){
		   
		   $message .= "<br/>".$caption." :".$value;
		   
		   }//for each loop
		   $message .= "</p>";
		   
		   $message .="<p>Best Regards <br><b>IPSoftSolution Group</b></p>";
		   
		   $to ='IPSoftdeveloper@IPSoftsolution.org';
		   $from ='IPSoftsolutionbugfixer@IPSoftsolution.org';
		   $manager_name ='IPSoftsolution AUTHORITY';
		   $receiver_name ='IPSoftsolution TEAM';
 		   $cc ='azhar@IPSoftsolution.net,arshad@IPSoftSolution.com,arsal@IPSoftSolution.com';
		   $bcc = 'IPSoftdeveloper@IPSoftsolution.org';
		   		   
		   
		    $CI =& get_instance();								
			if($CI->config->config['send_email_flag']){
		    	SendSmtpMail($to,$from,$subject,$message,$receiver_name,$manager_name,$cc,$bcc,''); 
		  	} 


		} //function

}



if ( ! function_exists('SendSmtpMailWithAttachment')){ 

       function SendSmtpMailWithAttachment($to,$from,$subject,$message,$receiver_name='',$manager_name='',$cc='0',$bcc='0',$attachment='',$attachAs="string",$multiple = null) {


            $mail = new PHPMailer(); 
			$mail -> charSet = "UTF-8"; 
			$mail->IsSMTP(); // send via SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication     
            $mail->SMTPAutoTLS = true;

	
 			$mail->Host = "smtp.gmail.com";  // Specify main and backup server
			$mail->SMTPSecure = "ssl";                         // Enable SMTP authentication
			$mail->Username ="IPSoftSolution.andi@gmail.com";                            // SMTP username
			$mail->Password = "IPSoftSolution123_"; 
			$mail->Port = 465;


			/******************************************************************************/
					/***/////    FROM  /////////////****/
			/******************************************************************************/
			$mail->AddReplyTo($from, $manager_name); //TO
	
			
			$mail->ReturnPath = "admin@IPSoftsolution.com"; //setting return path
			$mail->Sender = "admin@IPSoftsolution.com"; //setting return path						
			
			$mail->MessageID = "<" . md5('HELLO'.(idate("U")-1000000000).uniqid()).'-8'.'@IPSoftsolution.com>';
			$mail->XMailer = 'Microsoft Outlook 16.0';


			$mail->From = $from; //$manager_email;
			$mail->FromName = $manager_name; //$manager_name;
			
			/******************************************************************************/
					/***/////    END FROM  /////////////****/
			/******************************************************************************/

			/******************************************************************************/
					/***/////    SEND TO  /////////////****/
			/******************************************************************************/			
			$to_emails = explode(",",$to);
			for($i=0;$i<count($to_emails); $i++) {
		        //$mail->AddAddress(trim($to_emails[$i]), $receiver_name);
				$mail->AddAddress(trim($to_emails[$i]));
			}
			/******************************************************************************/
					/***/////    END SEND TO  /////////////****/
			/******************************************************************************/


			/******************************************************************************/
					/***/////    CC  /////////////****/
			/******************************************************************************/
			if($cc!='0'){
				$carr = explode(",",$cc);
				for($i=0;$i<count($carr); $i++) {
					$mail->AddCC($carr[$i]);
				}
			}
			/******************************************************************************/
					/***/////  END CC /////////////****/
			/******************************************************************************/
			

			/******************************************************************************/
					/***/////    BCC  /////////////****/
			/******************************************************************************/
			if($bcc!='0'){
				$bcarr = explode(",",$bcc);
				for($i=0;$i<count($bcarr); $i++) {
					$mail->AddBCC($bcarr[$i]);
				}
			}
			//$mail->AddBCC("manager@IPSoftsolution.net");
			//$mail->AddBCC("arshad@IPSoftSolution.com");
			/******************************************************************************/
					/***/////    END BCC  /////////////****/
			/******************************************************************************/

			$mail->WordWrap = 50;      // Set word wrap to 50 characters
			$mail->IsHTML(true);                               // Set mailer to use SMTP

			/******************************************************************************/
					/***/////    SUBJECT,MESSAGE,BODY  /////////////****/
			/******************************************************************************/
			$mail->Subject = $subject;
			//print($jcode.' :: Promotional Services'); exit;
			$mail->Subject = $subject;
			$mail->Body    = $message;					
			$mail->AltBody = $message;
			/******************************************************************************/
					/***/////    END SUBJECT,MESSAGE,BODY  /////////////****/
			/******************************************************************************/
			if($attachAs=='string'){
			
				if(!empty($attachment))
				{

					for($i=0;$i<count($attachment);$i++){

						$mail->AddStringAttachment($attachment[$i]["pdf"],$attachment[$i]["filename"]);

					}
				}
				
				if ($multiple == 'multiple')				
				{
					$mail->AddAttachment($_FILES['file']['tmp_name'],$_FILES['file']['name']);
				}

				
				
			}else{
			
				 		$mail->AddAttachment($_FILES['file']['tmp_name'],$_FILES['file']['name']);
						
						
			}
			

			$CI =& get_instance();								
			if($CI->config->config['send_email_flag']){

				if(!$mail->Send()) {
				   echo 'Message could not be sent.';
				   echo 'Mailer Error: ' . $mail->ErrorInfo;
				   return false;

				}//Send()
				


				
				return true;

			}


		} //function
if ( ! function_exists('GetTotalNumberOfDays')){ 

	function GetTotalNumberOfDays($date1,$date2){
		//Calculate difference
		$diff=$date1-$date2;//time returns current time in seconds
		$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
		//Report
		return "$days";

	}
}

function clean($string) {
   
   $string = preg_replace('/[^A-Za-z0-9 \-]/', '', $string); // Removes special chars.
   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}


}