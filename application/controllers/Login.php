<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->library('session');
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function index() {
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }else {
            redirect(site_url('home/login'), 'refresh');
        }
    }

    public function validate_login($from = "") {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $credential = array('email' => $email, 'password' => sha1($password), 'status' => 1);


        $this->db->select('*')->from('users');
        $this->db->where($credential);
//        $this->db->where('((email="'.$email.'" OR phone = "'.$email.'") AND password ="'.sha1($password).'")');
//        $this->db->where('status = 1');
//                 echo $this->db->get_compiled_select(); exit;

        $query = $this->db->get(); // get query result


        if ($query->num_rows() > 0) {
            $row = $query->row();
            //print_r($row);exit;
            $this->session->set_userdata('user_id', $row->id);
            $this->session->set_userdata('role_id', $row->role_id);
            $this->session->set_userdata('role', get_user_role('user_role', $row->id));
            $this->session->set_userdata('name', $row->first_name.' '.$row->last_name);
            $this->session->set_flashdata('flash_message', get_phrase('welcome').' '.$row->first_name.' '.$row->last_name);


            if ($row->role_id == 1) {
                $this->session->set_userdata('admin_login', '1');
                redirect(site_url('admin/dashboard'), 'refresh');
            }else if($row->role_id == 2){ 
                $this->session->set_userdata('user_login', '1');
  
                redirect(site_url('user/index'), 'refresh');
            }else if($row->role_id == 3){
                $this->session->set_userdata('user_login', '1');
                redirect(site_url('home/my_courses'), 'refresh');
            }else if($row->role_id == 4){
                $this->session->set_userdata('ogadmin', '1');
                $this->session->set_userdata('org', $row->org_id);
                redirect(site_url('OgAdmin/index'), 'refresh');
            }
        }else {
            $this->session->set_flashdata('error_message',get_phrase('invalid_login_credentials'));
            redirect(site_url('home/login'), 'refresh');
        }
    }

    public function register() {

        #check domain exists
        $domain = html_escape($this->input->post('domain'));
        $filter =" domain = '".$domain."' ";
        $domain_exists =  $this->crud_model->get_organizations($filter);
        
       
        if(empty($domain_exists[0]->domain)){     
            $this->session->set_flashdata('error_message', get_phrase('sorry_domain_not_available'));
            redirect(site_url('home/sign_up'), 'refresh');
        } 
       
       
        ## check phone exists
        $phone = html_escape($this->input->post('phone'));
        $phone_exists = $this->user_model->check_phone_exists($phone);
        if(!empty($phone_exists->id)){         
            $this->session->set_flashdata('error_message', get_phrase('sorry_phone_already_in_use'));
            redirect(site_url('home/sign_up'), 'refresh');
        } 

        $data['first_name'] = html_escape($this->input->post('first_name'));
        $data['last_name']  = html_escape($this->input->post('last_name'));
        $data['email']  = html_escape($this->input->post('email'));
        $data['phone']  = html_escape($this->input->post('phone'));
        $data['org_id']  = $domain_exists[0]->org_id;
        $data['password']  = sha1($this->input->post('password'));
        $data['pwd']  = $this->input->post('password');

        $verification_code =  md5(rand(100000000, 200000000));
        $data['verification_code'] = $verification_code;
        $data['status'] = 0;
        $data['wishlist'] = json_encode(array());
        $data['watch_history'] = json_encode(array());
        $data['date_added'] = strtotime(date("Y-m-d H:i:s"));
        $social_links = array(
            'facebook' => "",
            'twitter'  => "",
            'linkedin' => ""
        );
        $data['social_links'] = json_encode($social_links);
        $data['role_id']  = 3;
        //print_r($data);exit;
        // Add paypal keys
        $paypal_info = array();
        $paypal['production_client_id'] = "";
        array_push($paypal_info, $paypal);
        $data['paypal_keys'] = json_encode($paypal_info);
        // Add Stripe keys
        $stripe_info = array();
        $stripe_keys = array(
            'public_live_key' => "",
            'secret_live_key' => ""
        );
        array_push($stripe_info, $stripe_keys);
        $data['stripe_keys'] = json_encode($stripe_info);

        #### check email duplication 
        $validity = $this->user_model->check_duplication('on_create', $data['email']);
        if(empty($data['email'])){ $validity = 1; }

        
        if ($validity) {
            
            $this->user_model->register_user($data);                 
                //$this->email_model->send_email_verification_mail($data['email'], $this->input->post('password'));
                $this->session->set_flashdata('flash_message', get_phrase('need required activation from admin'));

        }else {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }
        redirect(site_url('home'), 'refresh');
    }

    public function logout($from = "") {
        //destroy sessions of specific userdata. We've done this for not removing the cart session
        $this->session_destroy();
        redirect(site_url('home/login'), 'refresh');
    }

    public function session_destroy() {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('name');
        if ($this->session->userdata('admin_login') == 1) {
            $this->session->unset_userdata('admin_login');
        }else {
            $this->session->unset_userdata('user_login');
        }
    }

    function forgot_password($from = "") {
        $email = $this->input->post('email');
        //resetting user password here
        $new_password = substr( md5( rand(100000000,20000000000) ) , 0,7);

        // Checking credential for admin
        $query = $this->db->get_where('users' , array('email' => $email));
        if ($query->num_rows() > 0)
        {
            $this->db->where('email' , $email);
            $this->db->update('users' , array('password' => sha1($new_password)));
            // send new password to user email
            $this->email_model->password_reset_email($new_password, $email);
            $this->session->set_flashdata('flash_message', get_phrase('please_check_your_email_for_new_password'));
            if ($from == 'backend') {
                redirect(site_url('login'), 'refresh');
            }else {
                redirect(site_url('home'), 'refresh');
            }
        }else {
            $this->session->set_flashdata('error_message', get_phrase('password_reset_failed'));
            if ($from == 'backend') {
                redirect(site_url('login'), 'refresh');
            }else {
                redirect(site_url('home'), 'refresh');
            }
        }
    }

    public function verify_email_address($verification_code = "") {
        $user_details = $this->db->get_where('users', array('verification_code' => $verification_code));
        if($user_details->num_rows() == 0) {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }else {
            $user_details = $user_details->row_array();
            $updater = array(
                'status' => 1
            );
            $this->db->where('id', $user_details['id']);
            $this->db->update('users', $updater);
            $this->session->set_flashdata('flash_message', get_phrase('congratulations').'!'.get_phrase('your_email_address_has_been_successfully_verified').'.');
        }
        redirect(site_url('home'), 'refresh');
    }
}
